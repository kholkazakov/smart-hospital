import DashboardLayout from "@/pages/Layout/DashboardLayout.vue";

import Dashboard from "@/pages/Dashboard.vue";
import Login from "@/pages/auth/Login";
import Users from "../pages/Users";
import Analyzes from "@/pages/Analyzes";
import Appointments from "@/pages/Appointments";
import MedicalCards from "@/pages/MedicalCards";
import Billings from "@/pages/Billings";
import Orders from "@/pages/Orders";
import Diseases from "@/pages/Diseases";
const routes = [
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/dashboard",
    children: [
      {
        path: "dashboard",
        name: "Dashboard",
        component: Dashboard
      },
      {
        path: "orders",
        name: "Orders",
        component: Orders
      },
      {
        path: "diseases",
        name: "Diseases",
        component: Diseases
      },
      {
        path: "analyzes",
        name: "Analyzes",
        component: Analyzes
      },
      {
        path: "users",
        name: "Users",
        component: Users
      },
      {
        path: "appointments",
        name: "Appointments",
        component: Appointments
      },
      {
        path: "medical-cards",
        name: "MedicalCards",
        component: MedicalCards
      },
      {
        path: "billings",
        name: "Billings",
        component: Billings
      }
    ]
  },
  { path: "/auth/login", component: Login },
  { path: "/logout", component: Login }
];

export default routes;
