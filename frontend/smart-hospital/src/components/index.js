// Cards
import ChartCard from "./Cards/ChartCard.vue";
import NavTabsCard from "./Cards/NavTabsCard.vue";
import StatsCard from "./Cards/StatsCard.vue";

// Tables
import NavTabsTable from "./Tables/NavTabsTable.vue";
import OrdersTable from "./Tables/OrdersTable.vue";
import SimpleTable from "./Tables/SimpleTable.vue";
import UsersTable from "./Tables/UsersTable";
import BillingsTable from "./Tables/BillingsTable";
import DiseasesTable from "./Tables/DiseasesTable";
import MedicalCardsTable from "./Tables/MedicalCardsTable";
import AppointmentsTable from "./Tables/AppointmentsTable";

export {
  ChartCard,
  NavTabsCard,
  StatsCard,
  NavTabsTable,
  OrdersTable,
  SimpleTable,
  BillingsTable,
  MedicalCardsTable,
  AppointmentsTable,
  DiseasesTable,
  UsersTable
};
