package uz.realsoft.smarthospital.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.realsoft.smarthospital.dto.ApiResponse;
import uz.realsoft.smarthospital.service.FileService;

import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/api/files")
public class FileController {
    final FileService fileService;

    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @PostMapping("/upload")
    public HttpEntity<?> addFile(@RequestParam("file") MultipartFile multipartFile) throws IOException {
        ApiResponse response = fileService.addFile(multipartFile, "category");
        return ResponseEntity.status(response.isSuccess()? HttpStatus.CREATED : HttpStatus.CONFLICT).body(response);
    }
    @GetMapping("/{id}")
    public HttpEntity<?> getFile(@PathVariable UUID id) throws IOException {
        return fileService.getFile(id);
    }
}
