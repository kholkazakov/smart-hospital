package uz.realsoft.smarthospital.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.realsoft.smarthospital.dto.ResBillingDto;
import uz.realsoft.smarthospital.entity.Billing;

@Repository
public interface BillingRepository extends JpaRepository<Billing,Long> {
    @Query(value =
            "select b.id, b.payment_num as paymentNum, b.payment_date as paymentDate, b.payment_sum as paymentSum, " +
            "b.order_id as orderId, b.disease_id as diseaseId, b.pay_type as payType, b.note, o.full_name as clientName " +
            "from billings b " +
            "left join orders o on o.id=b.order_id " +
            "where b.state=1",
            nativeQuery = true)
    Page<ResBillingDto> getBillings(Pageable pageable);
}
