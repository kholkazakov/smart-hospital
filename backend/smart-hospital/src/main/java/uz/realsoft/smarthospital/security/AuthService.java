package uz.realsoft.smarthospital.security;


import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.realsoft.smarthospital.dto.ApiResponse;
import uz.realsoft.smarthospital.dto.ResPageable;
import uz.realsoft.smarthospital.dto.ResUserDto;
import uz.realsoft.smarthospital.dto.UserDto;
import uz.realsoft.smarthospital.entity.User;
import uz.realsoft.smarthospital.entity.enums.RoleName;
import uz.realsoft.smarthospital.repository.FileRepository;
import uz.realsoft.smarthospital.repository.RoleRepository;
import uz.realsoft.smarthospital.repository.SpecializationRepository;
import uz.realsoft.smarthospital.repository.UserRepository;
import uz.realsoft.smarthospital.utils.CommonUtils;

import java.util.Collections;


@Service
@Slf4j
public class AuthService implements UserDetailsService {
    final UserRepository userRepository;
    final PasswordEncoder passwordEncoder;
    final RoleRepository roleRepository;
    final FileRepository fileRepository;
    final SpecializationRepository specializationRepository;


    public AuthService(UserRepository userRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository, FileRepository fileRepository, SpecializationRepository specializationRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.fileRepository = fileRepository;
        this.specializationRepository = specializationRepository;
    }


    public ResPageable getUsers(int page, int size) {
        Pageable pageable = CommonUtils.getPageable(page, size);
        Page<ResUserDto> users = userRepository.getUsers(pageable);
        return new ResPageable(
                users.getContent(),
                page,
                users.getTotalPages(),
                users.getTotalElements());
    }

    public ApiResponse register(UserDto userDto){
        try {
            User user= new User();
            setUserInfo(userDto, user);
            userRepository.save(user);
            return new ApiResponse("User saved!",true);
        } catch (Exception ignored){
            log.error(ignored.getMessage());
            return new ApiResponse("User not saved!",false);
        }

    }

    public ApiResponse editUser(Long id, UserDto userDto){
        try {
            User user = new User();
            user.setId(id);
            setUserInfo(userDto, user);
            userRepository.save(user);
            return new ApiResponse("User edited!",true);
        } catch (Exception ignored){
            log.error(ignored.getMessage());
            return new ApiResponse("User not edited!",false);
        }
    }

    private void setUserInfo(UserDto userDto, User user) {
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setExpierence(userDto.getExpierence());
        user.setPhone(userDto.getPhone());
        user.setGender(userDto.getGender());
        user.setAddress(userDto.getAddress());
        user.setUsername(userDto.getUsername());
        user.setBirthDate(userDto.getBirthDate());
        user.setStatus(userDto.getUserStatus());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setRoles(Collections.singletonList(roleRepository.findByName(RoleName.valueOf(userDto.getRole()))));
        user.setNote(userDto.getNote());
        user.setStatus(userDto.getUserStatus());
    }


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepository.findByUsername(s).orElseThrow(() -> new UsernameNotFoundException("User id no validate"));
    }

    public UserDetails loadUserByUserId(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("User id no validate"));
    }

}
