//CLASS CLASSDAN EXTENDS OLADI. FAQAT BIR MARTAGINA.
//CLASS INTERFACE DAN FAQATGINA IMPLEMENTS OLADI. ULARNING SONI CHEKLANMAGAN
//INTERFACE INTERFACEDAN EXTENDS OLADI. EXTENDSLAR SONI CHEKLANMAGAN.
// INTERFACE INTERFACEDAN IMPLEMENT OLA BILMAYDI.

package uz.realsoft.smarthospital.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.security.core.GrantedAuthority;
import uz.realsoft.smarthospital.entity.enums.RoleName;
import uz.realsoft.smarthospital.entity.template.MainEntity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Role extends MainEntity implements GrantedAuthority {

    @Enumerated(value = EnumType.STRING)
    private RoleName name;

    @Override
    public String getAuthority() {
        return name.name();
    }

}
