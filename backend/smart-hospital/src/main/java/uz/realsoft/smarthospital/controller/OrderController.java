package uz.realsoft.smarthospital.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.realsoft.smarthospital.dto.ApiResponse;
import uz.realsoft.smarthospital.dto.OrderDto;
import uz.realsoft.smarthospital.entity.Order;
import uz.realsoft.smarthospital.repository.OrderRepository;
import uz.realsoft.smarthospital.service.OrderService;
import uz.realsoft.smarthospital.utils.AppConstants;

@Controller
@RequestMapping("api/order")
public class OrderController {
    private final OrderService orderService;
    private final OrderRepository orderRepository;

    public OrderController(OrderService orderService, OrderRepository orderRepository) {
        this.orderService = orderService;
        this.orderRepository = orderRepository;
    }

    @PostMapping
    public HttpEntity<?> addOrder(@RequestBody OrderDto orderDto){
        ApiResponse response = orderService.addOrder(orderDto);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(response);
    }

    @PutMapping("{id}")
    public HttpEntity<?> editOrder(@PathVariable Long id, @RequestBody OrderDto orderDto){
        ApiResponse response = orderService.editOrder(id, orderDto);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(response);
    }

    @DeleteMapping("{id}")
    public HttpEntity<?> deleteOrder(@PathVariable Long id){
        try {
            orderRepository.deleteById(id);
            return ResponseEntity.status(200).body(new ApiResponse("Order deleted",true));
        } catch (Exception e){
            return ResponseEntity.status(200).body(new ApiResponse("Order not deleted",false));
        }
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getOrder(@PathVariable Long id){
        try {
            Order order = orderRepository.getById(id);
            return ResponseEntity.status(200).body(order);
        } catch (Exception e){
            return ResponseEntity.status(200).body(new ApiResponse("User not find",false));
        }
    }

    @GetMapping
    public HttpEntity<?> getOrders(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE) int page,
                                 @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_SIZE) int size){
        return ResponseEntity.ok(orderService.getOrders(page, size));
    }
}
