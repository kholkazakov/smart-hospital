package uz.realsoft.smarthospital.dto;

import lombok.Data;
import uz.realsoft.smarthospital.entity.enums.PayType;

import java.util.Date;

@Data
public class BillingDto {
    private String paymentNum;
    private Date paymentDate;
    private Double paymentSum;
    private Long orderId;
    private Long diseaseId;
    private PayType payType;
    private String note;
}
