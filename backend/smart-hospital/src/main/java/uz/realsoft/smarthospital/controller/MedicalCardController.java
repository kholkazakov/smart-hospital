package uz.realsoft.smarthospital.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.realsoft.smarthospital.dto.ApiResponse;
import uz.realsoft.smarthospital.dto.MedicalCardDto;
import uz.realsoft.smarthospital.entity.MedicalCard;
import uz.realsoft.smarthospital.repository.MedicalCardRepository;
import uz.realsoft.smarthospital.service.MedicalCardService;
import uz.realsoft.smarthospital.utils.AppConstants;

@Controller
@RequestMapping("api/medical_card")
public class MedicalCardController {
    private final MedicalCardService medicalCardService;
    private final MedicalCardRepository medicalCardRepository;

    public MedicalCardController(MedicalCardService medicalCardService, MedicalCardRepository medicalCardRepository) {
        this.medicalCardService = medicalCardService;
        this.medicalCardRepository = medicalCardRepository;
    }


    @PostMapping
    public HttpEntity<?> addMedicalCard(@RequestBody MedicalCardDto medicalCardDto){
        ApiResponse response = medicalCardService.addMedicalCard(medicalCardDto);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(response);
    }

    @PutMapping("{id}")
    public HttpEntity<?> editMedicalCard(@PathVariable Long id, @RequestBody MedicalCardDto medicalCardDto){
        ApiResponse response = medicalCardService.editMedicalCard(id, medicalCardDto);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(response);
    }

    @DeleteMapping("{id}")
    public HttpEntity<?> deleteMedicalCard(@PathVariable Long id){
        try {
            medicalCardRepository.deleteById(id);
            return ResponseEntity.status(200).body(new ApiResponse("Medical card deleted",true));
        } catch (Exception e){
            return ResponseEntity.status(200).body(new ApiResponse("Medical card not deleted",false));
        }
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getMedicalCard(@PathVariable Long id){
        try {
            MedicalCard medicalCard = medicalCardRepository.getById(id);
            return ResponseEntity.status(200).body(medicalCard);
        } catch (Exception e){
            return ResponseEntity.status(200).body(new ApiResponse("Medical card not find",false));
        }
    }

    @GetMapping
    public HttpEntity<?> getMedicalCards(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE) int page,
                                 @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_SIZE) int size){
        return ResponseEntity.ok(medicalCardService.getMedicalCards(page, size));
    }
}
