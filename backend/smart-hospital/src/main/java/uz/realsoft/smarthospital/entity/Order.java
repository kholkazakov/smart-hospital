package uz.realsoft.smarthospital.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.realsoft.smarthospital.entity.enums.Gender;
import uz.realsoft.smarthospital.entity.enums.OrderStatus;
import uz.realsoft.smarthospital.entity.template.MainEntity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "orders")
public class Order extends MainEntity {
    private String fullName;
    private String passport;
    private Date birthDate;
    private Date joinDate;
    private String address;
    private String phone;
    @Enumerated(EnumType.STRING)
    private Gender gender;
    private String note;
    @ManyToOne
    private User doctor;
    @ManyToOne
    private Disease disease;
    private Date expireDate;
    private Double paySum;
    private String docNum;
    private Date docDate;
    @Enumerated(EnumType.STRING)
    private OrderStatus status;
}
