package uz.realsoft.smarthospital.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.realsoft.smarthospital.dto.ApiResponse;
import uz.realsoft.smarthospital.dto.AppointmentDto;
import uz.realsoft.smarthospital.dto.ResAppointmentDto;
import uz.realsoft.smarthospital.dto.ResPageable;
import uz.realsoft.smarthospital.entity.Appointment;
import uz.realsoft.smarthospital.entity.enums.AppointmentStatus;
import uz.realsoft.smarthospital.repository.*;
import uz.realsoft.smarthospital.utils.CommonUtils;

import javax.transaction.Transactional;

@Service
public class AppointmentService {
    private final AppointmentRepository appointRepository;
    private final OrderRepository orderRepository;
    private final DiseaseRepository diseaseRepository;
    private final UserRepository userRepository;
    private final MedicalCardRepository medicalCardRepository;

    public AppointmentService(AppointmentRepository appointRepository, OrderRepository orderRepository, DiseaseRepository diseaseRepository, UserRepository userRepository, MedicalCardRepository medicalCardRepository) {
        this.appointRepository = appointRepository;
        this.orderRepository = orderRepository;
        this.diseaseRepository = diseaseRepository;
        this.userRepository = userRepository;
        this.medicalCardRepository = medicalCardRepository;
    }
    @Transactional
    public ApiResponse addAppoint(AppointmentDto appointDto){
        Appointment appointment = new Appointment();
        try {
            setAppointmentInfo(appointDto,appointment);
            appointment.setStatus(AppointmentStatus.PENDING);
            appointRepository.save(appointment);
            return new ApiResponse("Appoint saved succesfully",true);
        } catch (Exception e){
            return new ApiResponse("Appoint not saved",false);
        }

    }

    public ResPageable getAppointments(int page, int size) {
        Pageable pageable = CommonUtils.getPageable(page, size);
        Page<ResAppointmentDto> appointments = appointRepository.getAppointments(pageable);
        return new ResPageable(
                appointments.getContent(),
                page,
                appointments.getTotalPages(),
                appointments.getTotalElements());
    }

    @Transactional
    public ApiResponse editAppointment(Long id, AppointmentDto appointDto){
        Appointment appointment = new Appointment();
        try {
            setAppointmentInfo(appointDto,appointment);
            appointment.setId(id);
            appointment.setStatus(appointDto.getStatus());
            appointRepository.save(appointment);
            return new ApiResponse("Appoint edited succesfully",true);
        } catch (Exception e){
            return new ApiResponse("Appoint not edited",false);
        }

    }

    private void setAppointmentInfo(AppointmentDto appointDto, Appointment appointment) {
        appointment.setOrder(orderRepository.getById(appointDto.getOrderId()));
        appointment.setMedicalCard(medicalCardRepository.getById(appointDto.getMedicalCardId()));
        appointment.setDocDate(appointDto.getDocDate());
        appointment.setDoctor(userRepository.getById(appointDto.getDoctorId()));
        appointment.setStatus(appointDto.getStatus());
        appointment.setNote(appointDto.getNote());
    }
}
