package uz.realsoft.smarthospital.dto;

public interface ListDto {
    Long getId();
    String getFullName();

}
