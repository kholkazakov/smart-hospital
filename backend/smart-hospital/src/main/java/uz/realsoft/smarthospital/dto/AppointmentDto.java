package uz.realsoft.smarthospital.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.realsoft.smarthospital.entity.enums.AppointmentStatus;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppointmentDto {
    private Long orderId;
    private Long medicalCardId;
    private Date docDate;
    private Long doctorId;
    private AppointmentStatus status;
    private String note;
}
