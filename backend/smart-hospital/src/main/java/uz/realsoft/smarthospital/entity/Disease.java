package uz.realsoft.smarthospital.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.realsoft.smarthospital.entity.enums.Status;
import uz.realsoft.smarthospital.entity.template.MainEntity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name="diseases")
public class Disease extends MainEntity {
    private Double paySum;
    private String name;
    @Enumerated(EnumType.STRING)
    private Status status;
}
