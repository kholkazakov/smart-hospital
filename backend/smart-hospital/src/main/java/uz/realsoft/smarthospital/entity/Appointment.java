package uz.realsoft.smarthospital.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.realsoft.smarthospital.entity.enums.AppointmentStatus;
import uz.realsoft.smarthospital.entity.template.MainEntity;

import javax.persistence.*;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "appointments")
public class Appointment extends MainEntity {
    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private Order order;

    @ManyToOne
    private MedicalCard medicalCard;

    @ManyToOne
    private User doctor;

    private Date docDate;

    @Enumerated(EnumType.STRING)
    private AppointmentStatus status;
    private String note;
}
