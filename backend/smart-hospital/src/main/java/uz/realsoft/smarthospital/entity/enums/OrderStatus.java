package uz.realsoft.smarthospital.entity.enums;

public enum OrderStatus {
    CREATED,IN_PROCESS,IN_DOCTOR, ACCEPTED
}
