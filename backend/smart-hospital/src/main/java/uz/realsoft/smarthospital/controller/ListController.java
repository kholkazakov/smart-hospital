package uz.realsoft.smarthospital.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.realsoft.smarthospital.dto.ListDto;
import uz.realsoft.smarthospital.dto.ResDiseaseDto;
import uz.realsoft.smarthospital.repository.DiseaseRepository;
import uz.realsoft.smarthospital.repository.MedicalCardRepository;
import uz.realsoft.smarthospital.repository.OrderRepository;
import uz.realsoft.smarthospital.repository.UserRepository;

import java.util.List;

@RestController
@RequestMapping("/api/list")
public class ListController {
    private final UserRepository userRepository;
    private final DiseaseRepository diseaseRepository;
    private final OrderRepository orderRepository;
    private final MedicalCardRepository medicalCardRepository;

    public ListController(UserRepository userRepository, DiseaseRepository diseaseRepository, OrderRepository orderRepository, MedicalCardRepository medicalCardRepository) {
        this.userRepository = userRepository;
        this.diseaseRepository = diseaseRepository;
        this.orderRepository = orderRepository;
        this.medicalCardRepository = medicalCardRepository;
    }

    @GetMapping("doctors")
    public HttpEntity<?> getDoctors(){
        List<ListDto> doctors = userRepository.getDoctors();
        return ResponseEntity.ok(doctors);

    }

    @GetMapping("diseases")
    public HttpEntity<?> getDiseases(){
        List<ResDiseaseDto> diseases = diseaseRepository.getDiseases();
        return ResponseEntity.ok(diseases);
    }

    @GetMapping("orders")
    public HttpEntity<?> getOrders(){
        List<ListDto> orders = orderRepository.getOrderLists();
        return ResponseEntity.ok(orders);
    }
    @GetMapping("medical_cards")
    public HttpEntity<?> getMedicalCards(){
        List<ListDto> medicalCardsLists = medicalCardRepository.getMedicalCardsLists();
        return ResponseEntity.ok(medicalCardsLists);
    }
}
