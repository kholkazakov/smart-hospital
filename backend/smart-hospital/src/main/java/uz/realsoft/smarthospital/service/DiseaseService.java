package uz.realsoft.smarthospital.service;

import org.springframework.stereotype.Service;
import uz.realsoft.smarthospital.dto.ApiResponse;
import uz.realsoft.smarthospital.dto.DiseaseDto;
import uz.realsoft.smarthospital.entity.Disease;
import uz.realsoft.smarthospital.repository.DiseaseRepository;

@Service
public class DiseaseService {
    private final DiseaseRepository diseaseRepository;

    public DiseaseService(DiseaseRepository diseaseRepository) {
        this.diseaseRepository = diseaseRepository;
    }

    public ApiResponse addDisease(DiseaseDto diseaseDto){
        Disease disease = new Disease();
        try {
            disease.setName(diseaseDto.getName());
            disease.setPaySum(diseaseDto.getPaySum());
            disease.setStatus(diseaseDto.getStatus());
            diseaseRepository.save(disease);
            return new ApiResponse("Disease saved succesfully",true);
        } catch (Exception e){
            return new ApiResponse("Disease not saved",false);
        }
    }

    public ApiResponse editDisease(Long id,DiseaseDto diseaseDto){
        Disease disease = new Disease();
        try {
            disease.setName(diseaseDto.getName());
            disease.setPaySum(diseaseDto.getPaySum());
            disease.setStatus(diseaseDto.getStatus());
            disease.setId(id);
            diseaseRepository.save(disease);
            return new ApiResponse("Disease edited succesfully",true);
        } catch (Exception e){
            return new ApiResponse("Disease not edited",false);
        }
    }
}
