package uz.realsoft.smarthospital.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.realsoft.smarthospital.entity.Specialization;

@Repository
public interface SpecializationRepository extends JpaRepository<Specialization,Long> {
}
