package uz.realsoft.smarthospital.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JwtResponse {
    private String tokenType="Token";
    private String tokenBody;

    public JwtResponse(String tokenBody) {
        this.tokenBody = tokenBody;
    }
}
