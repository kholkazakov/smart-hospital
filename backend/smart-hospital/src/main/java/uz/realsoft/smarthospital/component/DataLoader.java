package uz.realsoft.smarthospital.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.realsoft.smarthospital.entity.User;
import uz.realsoft.smarthospital.entity.enums.RoleName;
import uz.realsoft.smarthospital.repository.RoleRepository;
import uz.realsoft.smarthospital.repository.UserRepository;

import java.util.Collections;


@Component
public class DataLoader implements CommandLineRunner {
    @Value("${spring.sql.init.mode}")
    private String initialMode;
    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;

    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")) {
            userRepository.save(
                    new User(
                            "Aziz",
                            "Xolkazakov",
                            "root",
                            passwordEncoder.encode("0099"),
                            Collections.singletonList(roleRepository.findByName(RoleName.SUPER_ADMIN))
                            )
            );
        }
    }
}
