package uz.realsoft.smarthospital.entity.enums;

public enum Gender {
    MALE, FEMALE
}