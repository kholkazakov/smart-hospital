package uz.realsoft.smarthospital.utils;


public interface ResOrder {
    Long getOrderNum();
    String getCustomName();
    String getCustomPhone();
    String getAddress();
    Integer getOrderStatus();
    Integer getPaymentStatus();
    String getOrderDate();
    Double getTotalSum();
}