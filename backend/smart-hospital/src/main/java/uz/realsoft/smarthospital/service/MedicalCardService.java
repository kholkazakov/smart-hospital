package uz.realsoft.smarthospital.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.realsoft.smarthospital.dto.ApiResponse;
import uz.realsoft.smarthospital.dto.MedicalCardDto;
import uz.realsoft.smarthospital.dto.ResMedicalCardDto;
import uz.realsoft.smarthospital.dto.ResPageable;
import uz.realsoft.smarthospital.entity.MedicalCard;
import uz.realsoft.smarthospital.repository.DiseaseRepository;
import uz.realsoft.smarthospital.repository.MedicalCardRepository;
import uz.realsoft.smarthospital.repository.UserRepository;
import uz.realsoft.smarthospital.utils.CommonUtils;

@Service
public class MedicalCardService {
    private final MedicalCardRepository medicalCardRepository;
    private final UserRepository userRepository;
    private final DiseaseRepository diseaseRepository;

    public MedicalCardService(MedicalCardRepository medicalCardRepository, UserRepository userRepository, DiseaseRepository diseaseRepository) {
        this.medicalCardRepository = medicalCardRepository;
        this.userRepository = userRepository;
        this.diseaseRepository = diseaseRepository;
    }

    public ApiResponse addMedicalCard(MedicalCardDto orderDto){
        MedicalCard medicalCard = new MedicalCard();
        try {
            setMedicalCardInfo(orderDto,medicalCard);
            medicalCardRepository.save(medicalCard);
            return new ApiResponse("MedicalCard saved succesfully",true);
        } catch (Exception e){
            return new ApiResponse("MedicalCard not saved",false);
        }
    }

    public ResPageable getMedicalCards(int page, int size) {
        Pageable pageable = CommonUtils.getPageable(page, size);
        Page<ResMedicalCardDto> medicalCard = medicalCardRepository.getMedicalCards(pageable);
        return new ResPageable(
                medicalCard.getContent(),
                page,
                medicalCard.getTotalPages(),
                medicalCard.getTotalElements());
    }

    public ApiResponse editMedicalCard(Long id,MedicalCardDto orderDto){
        MedicalCard medicalCard = new MedicalCard();
        try {
            setMedicalCardInfo(orderDto,medicalCard);
            medicalCardRepository.save(medicalCard);
            return new ApiResponse("MedicalCard edited succesfully",true);
        } catch (Exception e){
            return new ApiResponse("MedicalCard not edited",false);
        }

    }

    private void setMedicalCardInfo(MedicalCardDto medicalCardDto, MedicalCard medicalCard) {
        medicalCard.setFullName(medicalCardDto.getFullName());
        medicalCard.setPassport(medicalCardDto.getPassport());
        medicalCard.setBirthDate(medicalCardDto.getBirthDate());
        medicalCard.setAddress(medicalCardDto.getAddress());
        medicalCard.setPhone(medicalCardDto.getPhone());
        medicalCard.setGender(medicalCardDto.getGender());
        medicalCard.setNote(medicalCardDto.getNote());
        medicalCard.setStatus(medicalCardDto.getStatus());
    }
}
