package uz.realsoft.smarthospital.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.realsoft.smarthospital.dto.ApiResponse;
import uz.realsoft.smarthospital.dto.AppointmentDto;
import uz.realsoft.smarthospital.entity.Appointment;
import uz.realsoft.smarthospital.repository.AppointmentRepository;
import uz.realsoft.smarthospital.service.AppointmentService;
import uz.realsoft.smarthospital.utils.AppConstants;

@Controller
@RequestMapping("/api/appointment")
public class AppointmentController {
    private final AppointmentService appointService;
    private final AppointmentRepository appointRepository;

    public AppointmentController(AppointmentService appointService, AppointmentRepository appointRepository) {
        this.appointService = appointService;
        this.appointRepository = appointRepository;
    }


    @PostMapping
    public HttpEntity<?> addAppoint(@RequestBody AppointmentDto appointDto){
        ApiResponse response = appointService.addAppoint(appointDto);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(response);
    }

    @PutMapping("{id}")
    public HttpEntity<?> editAppoint(@PathVariable Long id, @RequestBody AppointmentDto appointDto){
        ApiResponse response = appointService.editAppointment(id, appointDto);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(response);
    }

    @DeleteMapping("{id}")
    public HttpEntity<?> deleteAppointment(@PathVariable Long id){
        try {
            appointRepository.deleteById(id);
            return ResponseEntity.status(200).body(new ApiResponse("Appointment deleted",true));
        } catch (Exception e){
            return ResponseEntity.status(200).body(new ApiResponse("Appointment not deleted",false));
        }
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getAppointment(@PathVariable Long id){
        try {
            Appointment appointment = appointRepository.getById(id);
            return ResponseEntity.status(200).body(appointment);
        } catch (Exception e){
            return ResponseEntity.status(200).body(new ApiResponse("Appointment not find",false));
        }
    }

    @GetMapping
    public HttpEntity<?> getAppointments(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE) int page,
                                   @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_SIZE) int size){
        return ResponseEntity.ok(appointService.getAppointments(page, size));
    }
}
