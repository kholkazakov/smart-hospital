package uz.realsoft.smarthospital.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.realsoft.smarthospital.dto.ApiResponse;
import uz.realsoft.smarthospital.repository.OrderRepository;

@RestController
@RequestMapping("api/statistic")
public class StatisticController {
    private final OrderRepository orderRepository;

    public StatisticController(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @GetMapping("all")
    public ApiResponse getAllStatistic(){
        orderRepository.getOrderStatistic();
        return new ApiResponse("All statistic",true);
    }
}
