package uz.realsoft.smarthospital.dto;

public interface ResDiseaseDto {
    Long getId();
    String getName();
    Double getPaySum();
    String getStatus();
}
