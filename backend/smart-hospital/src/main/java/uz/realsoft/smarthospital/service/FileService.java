package uz.realsoft.smarthospital.service;

import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.realsoft.smarthospital.dto.ApiResponse;
import uz.realsoft.smarthospital.entity.File;
import uz.realsoft.smarthospital.repository.FileRepository;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@Service
public class FileService {
    final FileRepository fileRepository;

    public FileService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public ApiResponse addFile(MultipartFile multipartFile, String type) throws IOException {
        File file=new File();
        String path="src/main/resources/static/images/" + multipartFile.getOriginalFilename();
        File file1=new File(path);
        OutputStream os=new FileOutputStream(String.valueOf(file1));
        byte[] fileBytes = multipartFile.getBytes();
        os.write(multipartFile.getBytes());
        file.setContentType(multipartFile.getContentType());
        file.setExtension(multipartFile.getOriginalFilename().split("\\.")[1]);
        file.setName(multipartFile.getOriginalFilename());
        file.setPath(path);
        file.setSize(multipartFile.getSize());
        fileRepository.save(file);
        return new ApiResponse("File saved successfully",true);
    }

    public HttpEntity<?> getFile(UUID id) throws IOException {
        File attachment = fileRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getFile"));
        java.io.File file = new java.io.File(attachment.getPath());
        byte[] allBytes = Files.readAllBytes(file.toPath());
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(attachment.getContentType()))
                .body(Files.readAllBytes(Paths.get(attachment.getPath())));
    }
}
