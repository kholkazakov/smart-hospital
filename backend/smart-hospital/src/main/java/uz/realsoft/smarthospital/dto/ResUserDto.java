package uz.realsoft.smarthospital.dto;

import uz.realsoft.smarthospital.entity.enums.Gender;
import uz.realsoft.smarthospital.entity.enums.RoleName;
import uz.realsoft.smarthospital.entity.enums.Status;

import java.util.Date;

public interface ResUserDto {
    Long getId();
    String getFirstName();
    String getLastName();
    String getPhone();
    Date getBirthDate();
    Integer getExpierence();
    Gender getGender();
    String getAddress();
    String getUsername();
    Long getSpecializationId();
    String getNote();
    RoleName getRole();
    Status getUserStatus();
}
