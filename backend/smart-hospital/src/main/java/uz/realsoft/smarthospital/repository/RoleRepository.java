package uz.realsoft.smarthospital.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.realsoft.smarthospital.entity.Role;
import uz.realsoft.smarthospital.entity.enums.RoleName;

@RepositoryRestResource(path = "role")
public interface RoleRepository extends JpaRepository<Role,Integer> {
    Role findByName(RoleName roleUser);
}
//Role data-rest