package uz.realsoft.smarthospital.dto;

import lombok.Data;

@Data
public class ReqLogin {
    private String username;
    private String password;
}
