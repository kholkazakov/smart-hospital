package uz.realsoft.smarthospital.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.realsoft.smarthospital.dto.ApiResponse;
import uz.realsoft.smarthospital.dto.BillingDto;
import uz.realsoft.smarthospital.dto.ResBillingDto;
import uz.realsoft.smarthospital.dto.ResPageable;
import uz.realsoft.smarthospital.entity.Billing;
import uz.realsoft.smarthospital.entity.enums.OrderStatus;
import uz.realsoft.smarthospital.repository.BillingRepository;
import uz.realsoft.smarthospital.repository.DiseaseRepository;
import uz.realsoft.smarthospital.repository.OrderRepository;
import uz.realsoft.smarthospital.utils.CommonUtils;

import javax.transaction.Transactional;

@Service
public class BillingService {
    private final BillingRepository billingRepository;
    private final OrderRepository orderRepository;
    private final DiseaseRepository diseaseRepository;

    public BillingService(BillingRepository billingRepository, OrderRepository orderRepository, DiseaseRepository diseaseRepository) {
        this.billingRepository = billingRepository;
        this.orderRepository = orderRepository;
        this.diseaseRepository = diseaseRepository;
    }
    @Transactional
    public ApiResponse addBilling(BillingDto billingDto){
        Billing billing = new Billing();
        try {
            setBillingInfo(billingDto,billing);
            billingRepository.save(billing);
            orderRepository.setOrderStatus(billingDto.getOrderId(), OrderStatus.IN_PROCESS.toString());
            return new ApiResponse("Billing saved succesfully",true);
        } catch (Exception e){
            return new ApiResponse("Billing not saved",false);
        }
    }

    public ResPageable getBillings(int page, int size) {
        Pageable pageable = CommonUtils.getPageable(page, size);
        Page<ResBillingDto> billings = billingRepository.getBillings(pageable);
        return new ResPageable(
                billings.getContent(),
                page,
                billings.getTotalPages(),
                billings.getTotalElements());
    }
    @Transactional
    public ApiResponse editBilling(Long id,BillingDto billingDto){
        Billing billing = new Billing();
        try {
            setBillingInfo(billingDto,billing);
            billing.setId(id);
            billingRepository.save(billing);
            orderRepository.setOrderStatus(billingDto.getOrderId(), OrderStatus.IN_PROCESS.toString());
            return new ApiResponse("Billing edited succesfully",true);
        } catch (Exception e){
            return new ApiResponse("Billing not edited",false);
        }
    }

    private void setBillingInfo(BillingDto billingDto, Billing billing) {
        billing.setPaymentNum(billingDto.getPaymentNum());
        billing.setPaymentDate(billingDto.getPaymentDate());
        billing.setPaymentSum(billingDto.getPaymentSum());
        billing.setOrder(orderRepository.getById(billingDto.getOrderId()));
        billing.setDisease(diseaseRepository.getById(billingDto.getDiseaseId()));
        billing.setPayType(billingDto.getPayType());
        billing.setNote(billingDto.getNote());
    }
}
