package uz.realsoft.smarthospital.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.realsoft.smarthospital.dto.ApiResponse;
import uz.realsoft.smarthospital.dto.DiseaseDto;
import uz.realsoft.smarthospital.entity.Disease;
import uz.realsoft.smarthospital.repository.DiseaseRepository;
import uz.realsoft.smarthospital.service.DiseaseService;

@RestController
@RequestMapping("api/disease")
public class DiseaseController {
    private final DiseaseService diseaseService;
    private final DiseaseRepository diseaseRepository;

    public DiseaseController(DiseaseService diseaseService, DiseaseRepository diseaseRepository) {
        this.diseaseService = diseaseService;
        this.diseaseRepository = diseaseRepository;
    }

    @PostMapping
    public HttpEntity<?> addDisease(@RequestBody DiseaseDto diseaseDto){
        ApiResponse response = diseaseService.addDisease(diseaseDto);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(response);
    }

    @PutMapping("{id}")
    public HttpEntity<?> editDisease(@PathVariable Long id, @RequestBody DiseaseDto diseaseDto){
        ApiResponse response = diseaseService.editDisease(id,diseaseDto);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(response);
    }

    @DeleteMapping("{id}")
    public HttpEntity<?> deleteDisease(@PathVariable Long id){
        try {
            diseaseRepository.deleteById(id);
            return ResponseEntity.status(200).body(new ApiResponse("Disease deleted",true));
        } catch (Exception e){
            return ResponseEntity.status(200).body(new ApiResponse("Disease not deleted",false));
        }
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getDisease(@PathVariable Long id){
        try {
            Disease disease = diseaseRepository.getById(id);
            return ResponseEntity.status(200).body(disease);
        } catch (Exception e){
            return ResponseEntity.status(200).body(new ApiResponse("Disease not find",false));
        }
    }

    @GetMapping
    public HttpEntity<?> getDiseases(){
        return ResponseEntity.ok(diseaseRepository.getDiseases());
    }
}
