package uz.realsoft.smarthospital.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResFile {
    private String name;
    private byte[] bytes;
    private String contentType;
}
