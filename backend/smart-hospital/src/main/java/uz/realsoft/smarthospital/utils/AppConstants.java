package uz.realsoft.smarthospital.utils;

public interface AppConstants {

    String DEFAULT_PAGE = "0";
    String DEFAULT_SIZE = "10";
    int MAX_PAGE_SIZE=40;
    String SEARCH="all";
    String STATUS = "0";
    String CATEGORY = "0";
    String BEGIN_DATE = "1970-01-01";
    String END_DATE = "2025-09-26";

}
