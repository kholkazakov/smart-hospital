package uz.realsoft.smarthospital.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.realsoft.smarthospital.entity.File;

import java.util.UUID;

public interface FileRepository extends JpaRepository<File, UUID> {
}
