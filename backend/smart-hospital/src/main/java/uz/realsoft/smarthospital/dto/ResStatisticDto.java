package uz.realsoft.smarthospital.dto;

public interface ResStatisticDto {
   Integer getC1();
   Integer getC2();
   Integer getC3();
   Integer getC4();
}
