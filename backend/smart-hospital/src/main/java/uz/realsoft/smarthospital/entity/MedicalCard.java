package uz.realsoft.smarthospital.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.realsoft.smarthospital.entity.enums.Gender;
import uz.realsoft.smarthospital.entity.enums.Status;
import uz.realsoft.smarthospital.entity.template.MainEntity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "medical_cards")
public class MedicalCard extends MainEntity {
    private String fullName;
    private String passport;
    private Date birthDate;
    private String address;
    private String phone;
    @Enumerated(EnumType.STRING)
    private Gender gender;
    private String note;
    @Enumerated(EnumType.STRING)
    private Status status;
}
