package uz.realsoft.smarthospital.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.realsoft.smarthospital.dto.ResAppointmentDto;
import uz.realsoft.smarthospital.entity.Appointment;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment,Long> {

    @Query(value = "select a.id, a.order_id as orderId, o.disease_id as diseaseId, o.full_name as fullName, " +
                   "a.medical_card_id as medicalCardId, o.gender, a.doc_date as docDate, a.doctor_id as doctorId, " +
                   "a.status, a.note " +
                   "from appointments a " +
                   "left join orders o on o.id=a.order_id " +
                   "where a.state=1", nativeQuery = true)
    Page<ResAppointmentDto> getAppointments(Pageable pageable);
}
