package uz.realsoft.smarthospital.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.realsoft.smarthospital.dto.ApiResponse;
import uz.realsoft.smarthospital.dto.OrderDto;
import uz.realsoft.smarthospital.dto.ResOrderDto;
import uz.realsoft.smarthospital.dto.ResPageable;
import uz.realsoft.smarthospital.entity.Order;
import uz.realsoft.smarthospital.entity.enums.OrderStatus;
import uz.realsoft.smarthospital.repository.DiseaseRepository;
import uz.realsoft.smarthospital.repository.OrderRepository;
import uz.realsoft.smarthospital.repository.UserRepository;
import uz.realsoft.smarthospital.utils.CommonUtils;

import java.util.Date;

@Service
public class OrderService {
    private final OrderRepository orderRepository;
    private final UserRepository userRepository;
    private final DiseaseRepository diseaseRepository;

    public OrderService(OrderRepository orderRepository, UserRepository userRepository, DiseaseRepository diseaseRepository) {
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
        this.diseaseRepository = diseaseRepository;
    }

    public ApiResponse addOrder(OrderDto orderDto){
        Order order = new Order();
        try {
            setOrderInfo(orderDto,order);
            order.setStatus(OrderStatus.IN_DOCTOR);
            orderRepository.save(order);
            return new ApiResponse("Order saved succesfully",true);
        } catch (Exception e){
            return new ApiResponse("Order not saved",false);
        }
    }

    public ResPageable getOrders(int page, int size) {
        Pageable pageable = CommonUtils.getPageable(page, size);
        Page<ResOrderDto> orders = orderRepository.getOrders(pageable);
        return new ResPageable(
                orders.getContent(),
                page,
                orders.getTotalPages(),
                orders.getTotalElements());
    }

    public ApiResponse editOrder(Long id,OrderDto orderDto){
        Order order = new Order();
        try {
            setOrderInfo(orderDto,order);
            order.setStatus(OrderStatus.IN_DOCTOR);
            orderRepository.save(order);
            return new ApiResponse("Order edited succesfully",true);
        } catch (Exception e){
            return new ApiResponse("Order not edited",false);
        }

    }

    private void setOrderInfo(OrderDto orderDto, Order order) {
        order.setFullName(orderDto.getFullName());
        order.setPassport(orderDto.getPassport());
        order.setBirthDate(orderDto.getBirthDate());
        order.setJoinDate(new Date());
        order.setAddress(orderDto.getAddress());
        order.setPhone(orderDto.getPhone());
        order.setGender(orderDto.getGender());
        order.setNote(orderDto.getNote());
        order.setDoctor(userRepository.getById(orderDto.getDoctorId()));
        order.setDisease(diseaseRepository.getById(orderDto.getDiseaseId()));
        order.setPaySum(orderDto.getPaySum());
        order.setDocNum(orderDto.getDocNum());
        order.setDocDate(orderDto.getDocDate());
    }
}
