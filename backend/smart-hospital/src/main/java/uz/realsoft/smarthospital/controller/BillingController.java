package uz.realsoft.smarthospital.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.realsoft.smarthospital.dto.ApiResponse;
import uz.realsoft.smarthospital.dto.BillingDto;
import uz.realsoft.smarthospital.dto.ResPageable;
import uz.realsoft.smarthospital.repository.BillingRepository;
import uz.realsoft.smarthospital.service.BillingService;
import uz.realsoft.smarthospital.utils.AppConstants;

@RestController
@RequestMapping("/api/billing")
public class BillingController {
    private final BillingService billingService;
    private final BillingRepository billingRepository;

    public BillingController(BillingService billingService, BillingRepository billingRepository) {
        this.billingService = billingService;
        this.billingRepository = billingRepository;
    }

    @PostMapping
    public HttpEntity<?> addBilling(@RequestBody BillingDto billingDto){
        ApiResponse response = billingService.addBilling(billingDto);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(response);
    }

    @PutMapping("{id}")
    public HttpEntity<?> editBilling(@PathVariable Long id, @RequestBody BillingDto billingDto){
        ApiResponse response = billingService.editBilling(id,billingDto);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(response);
    }
    @GetMapping
    public HttpEntity<?> getBillings(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE) int page,
                                     @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_SIZE) int size){
        ResPageable billings = billingService.getBillings(page, size);
        return ResponseEntity.ok(billings);
    }

    @DeleteMapping("{id}")
    public HttpEntity<?> deleteBilling(@PathVariable Long id){
        try {
            billingRepository.deleteById(id);
            return ResponseEntity.status(200).body(new ApiResponse("Billing deleted",true));
        } catch (Exception e){
            return ResponseEntity.status(200).body(new ApiResponse("Billing not deleted",false));
        }
    }
}
