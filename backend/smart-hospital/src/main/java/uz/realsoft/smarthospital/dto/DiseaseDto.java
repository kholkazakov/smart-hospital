package uz.realsoft.smarthospital.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.realsoft.smarthospital.entity.enums.Status;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DiseaseDto {
    private String name;
    private Double paySum;
    private Status status;
}
