package uz.realsoft.smarthospital.entity.enums;

public enum AppointmentSource {
    ONLINE, OFFLINE
}
