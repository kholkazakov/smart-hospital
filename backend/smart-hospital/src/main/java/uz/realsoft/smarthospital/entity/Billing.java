package uz.realsoft.smarthospital.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.realsoft.smarthospital.entity.enums.PayType;
import uz.realsoft.smarthospital.entity.template.MainEntity;

import javax.persistence.*;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "billings")
public class Billing extends MainEntity {
    private String paymentNum;
    private Date paymentDate;
    private Double paymentSum;
    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private Order order;
    @ManyToOne
    private Disease disease;
    @Enumerated(EnumType.STRING)
    private PayType payType;
    private String note;
}
