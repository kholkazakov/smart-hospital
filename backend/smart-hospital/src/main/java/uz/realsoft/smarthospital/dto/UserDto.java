package uz.realsoft.smarthospital.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.realsoft.smarthospital.entity.enums.Gender;
import uz.realsoft.smarthospital.entity.enums.Status;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private String firstName;
    private String lastName;
    private String phone;
    private Date birthDate;
    private Integer expierence;
    private Gender gender;
    private String address;
    private String username;
    private String password;
    private Long specializationId;
    private String note;
    private String role;
    private Status userStatus;
}
