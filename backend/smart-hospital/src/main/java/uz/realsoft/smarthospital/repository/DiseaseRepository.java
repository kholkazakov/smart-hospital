package uz.realsoft.smarthospital.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.realsoft.smarthospital.dto.ResDiseaseDto;
import uz.realsoft.smarthospital.entity.Disease;

import java.util.List;

@Repository
public interface DiseaseRepository extends JpaRepository<Disease,Long> {

    @Query(value = "select id, name, pay_sum as paySum, status from diseases where state=1 order by name", nativeQuery = true)
    List<ResDiseaseDto> getDiseases();
}
