package uz.realsoft.smarthospital.dto;

import uz.realsoft.smarthospital.entity.enums.Gender;
import uz.realsoft.smarthospital.entity.enums.OrderStatus;

import java.util.Date;

public interface ResOrderDto {
    Long getId();
    String getFullName();
    String getPassport();
    Date getBirthDate();
    String getAddress();
    String getPhone();
    Gender getGender();
    String getNote();
    Long getDoctorId();
    Long getDiseaseId();
    Double getPaySum();
    String getDocNum();
    Date getDocDate();
    OrderStatus getStatus();
}
