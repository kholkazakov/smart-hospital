package uz.realsoft.smarthospital.dto;

import uz.realsoft.smarthospital.entity.enums.Gender;
import uz.realsoft.smarthospital.entity.enums.Status;

import java.util.Date;

public interface ResMedicalCardDto {
    String getFullName();
    String getPassport();
    Date getBirthDate();
    String getAddress();
    String getPhone();
    Gender getGender();
    String getNote();
    Status getStatus();
}
