package uz.realsoft.smarthospital.dto;

import java.util.Date;

public interface ResAppointmentDto {
    Long getId();
    Long getOrderId();
    Long getDiseaseId();
    String getFullName();
    Long getMedicalCardId();
    String getGender();
    Date getDocDate();
    Long getDoctorId();
    String getStatus();
    String getNote();
}
