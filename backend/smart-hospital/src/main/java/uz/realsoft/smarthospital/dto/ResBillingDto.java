package uz.realsoft.smarthospital.dto;

import uz.realsoft.smarthospital.entity.enums.PayType;

import java.util.Date;

public interface ResBillingDto {
    Long getId();
    String getClientName();
    String getPaymentNum();
    Date getPaymentDate();
    Double getPaymentSum();
    Long getOrderId();
    Long getDiseaseId();
    PayType getPayType();
    String getNote();
}
