package uz.realsoft.smarthospital.entity.enums;

public enum RoleName {
    SUPER_ADMIN, ADMIN, DOCTOR, PATHOLOGIST,RADIOLOGIST,RECEPTIONIST,PATIENT,NURSE,PHARMACIST,ACCOUNTANT
}