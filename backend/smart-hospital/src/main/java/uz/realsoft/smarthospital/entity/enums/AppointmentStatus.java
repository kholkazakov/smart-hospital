package uz.realsoft.smarthospital.entity.enums;

public enum AppointmentStatus {
    CANCELED, PENDING,APPROVED
}
