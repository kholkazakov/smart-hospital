package uz.realsoft.smarthospital.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.realsoft.smarthospital.dto.ListDto;
import uz.realsoft.smarthospital.dto.ResUserDto;
import uz.realsoft.smarthospital.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String s);

    boolean existsByUsername(String username);
    @Query(value =
            "select u.id, u.first_name as firstName, u.last_name as lastName, u.phone, u.birth_date as birthDate, u.expierence, u.gender, " +
            "u.address, u.username, u.specialization_id as specializationId, u.note, u.user_status as userStatus, r.name  as role " +
            "from users u " +
            "left join user_role ur on ur.user_id=u.id " +
            "left join role r on r.id=ur.role_id " +
            "where u.state=1 and u.id<>1",
            nativeQuery = true)
    Page<ResUserDto> getUsers(Pageable pageable);

    @Query(value =
            "select u.id, u.first_name || ' '  || u.last_name as fullName " +
            "from users u " +
            "left join user_role ur on ur.user_id=u.id " +
            "left join role r on r.id=ur.role_id " +
            "where u.state=1 and r.name in('DOCTOR', 'PATHOLOGIST','RADIOLOGIST')",
    nativeQuery = true)
    List<ListDto> getDoctors();
}
