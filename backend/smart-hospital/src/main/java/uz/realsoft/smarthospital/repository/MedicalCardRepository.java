package uz.realsoft.smarthospital.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.realsoft.smarthospital.dto.ListDto;
import uz.realsoft.smarthospital.dto.ResMedicalCardDto;
import uz.realsoft.smarthospital.entity.MedicalCard;

import java.util.List;

@Repository
public interface MedicalCardRepository extends JpaRepository<MedicalCard,Long> {

    @Query(value =
            "select m.id, m.full_name as fullName, m.passport, m.birth_date as birthDate, m.address, " +
            "m.phone, m.gender, m.note, m.status " +
            "from medical_cards m " +
            "where m.state=1",
            nativeQuery = true)
    Page<ResMedicalCardDto> getMedicalCards(Pageable pageable);

    @Query(value = "select id, id || ' - ' || full_name as fullName " +
                   "from medical_cards m where state=1 order by full_name", nativeQuery = true)
    List<ListDto> getMedicalCardsLists();
}
