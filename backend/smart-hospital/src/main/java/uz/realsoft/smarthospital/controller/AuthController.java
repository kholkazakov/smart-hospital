package uz.realsoft.smarthospital.controller;


import lombok.AllArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import uz.realsoft.smarthospital.dto.ApiResponse;
import uz.realsoft.smarthospital.dto.JwtResponse;
import uz.realsoft.smarthospital.dto.ReqLogin;
import uz.realsoft.smarthospital.dto.UserDto;
import uz.realsoft.smarthospital.entity.User;
import uz.realsoft.smarthospital.repository.UserRepository;
import uz.realsoft.smarthospital.security.AuthService;
import uz.realsoft.smarthospital.security.JwtTokenProvider;
import uz.realsoft.smarthospital.utils.AppConstants;

@AllArgsConstructor
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final AuthService authService;
    private final UserRepository userRepository;

    @PostMapping("/login")
    public HttpEntity<?> signIn(@RequestBody ReqLogin reqLogin) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(reqLogin.getUsername(), reqLogin.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtTokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtResponse(token));
    }

    @PostMapping("register")
    public HttpEntity<?> register(@RequestBody UserDto userDto){
        ApiResponse response = authService.register(userDto);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(response);
    }

    @PutMapping("user/{id}")
    public HttpEntity<?> editUser(@PathVariable Long id, @RequestBody UserDto userDto){
        ApiResponse response = authService.editUser(id,userDto);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(response);
    }

    @DeleteMapping("user/{id}")
    public HttpEntity<?> deleteUser(@PathVariable Long id){
        try {
            userRepository.deleteById(id);
            return ResponseEntity.status(200).body(new ApiResponse("User deleted",true));
        } catch (Exception e){
            return ResponseEntity.status(200).body(new ApiResponse("User not deleted",false));
        }
    }

    @GetMapping("user/{id}")
    public HttpEntity<?> getUser(@PathVariable Long id){
        try {
            User user = userRepository.getById(id);
            return ResponseEntity.status(200).body(user);
        } catch (Exception e){
            return ResponseEntity.status(200).body(new ApiResponse("User not find",false));
        }
    }

    @GetMapping("users")
    public HttpEntity<?> getUser(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE) int page,
                              @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_SIZE) int size){
        return ResponseEntity.ok(authService.getUsers(page, size));
    }



}
