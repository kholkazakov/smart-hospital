package uz.realsoft.smarthospital.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uz.realsoft.smarthospital.dto.ListDto;
import uz.realsoft.smarthospital.dto.OrderStatisticDto;
import uz.realsoft.smarthospital.dto.ResOrderDto;
import uz.realsoft.smarthospital.entity.Order;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order,Long> {
    @Query(value =
            "select id, full_name as fullName,passport, birth_date as birthDate, address, phone, gender, note, doctor_id as doctorId, " +
            "disease_id as diseaseId, pay_sum as paySum, doc_num as docNum, doc_date as docDate, status " +
            "from orders " +
            "where state=1",
            nativeQuery = true)
    Page<ResOrderDto> getOrders(Pageable pageable);

    @Modifying
    @Query(value = "update orders set status = :status where id=:id ",nativeQuery = true)
    void setOrderStatus(@Param("id") Long id, @Param("status") String status);

    @Query(value = "select id, doc_num || ' - ' || full_name as fullName " +
                   "from orders where state=1 order by full_name", nativeQuery = true)
    List<ListDto> getOrderLists();

    @Query(value = "select count(o.id) as oc1, " +
                    "sum(case when o.status='IN_PROCESS' then 1 else 0 end) as oc2, " +
                    "sum(case when o.status='ACCEPTED' then 1 else 0 end) as oc3, " +
                    "sum(case when o.status='REJECTED' then 1 else 0 end) as oc4 " +
                    "from orders o " +
                    "where o.state=1")
    OrderStatisticDto getOrderStatistic();
}
