package uz.realsoft.smarthospital.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.realsoft.smarthospital.entity.enums.Gender;
import uz.realsoft.smarthospital.entity.enums.OrderStatus;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto {
    private String fullName;
    private String passport;
    private Date birthDate;
    private String address;
    private String phone;

    private Gender gender;
    private String note;

    private Long doctorId;

    private Long diseaseId;
    private Double paySum;
    private String docNum;
    private Date docDate;

    private OrderStatus status;
}
